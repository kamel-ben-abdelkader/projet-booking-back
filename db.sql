DROP DATABASE IF EXISTS last_booking_db;
CREATE DATABASE last_booking_db;
USE last_booking_db;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`(  
    `id` int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    `firstname` VARCHAR(60)NOT NULL,
    `name` VARCHAR(60) NOT NULL,
    `email` VARCHAR (100) NOT NULL UNIQUE,
    `password` VARCHAR (255) NOT NULL,
    `adress` VARCHAR (255) NOT NULL,
    `phone` VARCHAR (40) DEFAULT NULL ,
    `role` VARCHAR (40) NOT NULL,
    `company` VARCHAR (40) DEFAULT NULL,
    `siret` VARCHAR (40) DEFAULT NULL
) DEFAULT CHARSET=utf8mb4 comment '';


DROP TABLE IF EXISTS `hotel`;
CREATE TABLE `hotel` (
    `id`  int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    `name`  VARCHAR(60) NOT NULL,
    `description` TEXT NOT NULL,
    `adress` VARCHAR (255) NOT NULL,
    `service` TEXT NOT NULL,
    `type` VARCHAR (252) NOT NULL,
    `picture` VARCHAR (252),
    `user_id` int,
     KEY `FK_UserHotel` (`user_id`),
    CONSTRAINT `FK_UserHotel` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE SET NULL  ON UPDATE SET NULL
) DEFAULT CHARSET=utf8mb4 comment '';


DROP TABLE IF EXISTS `room`;
CREATE TABLE `room` (
    `id`  int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    `name` TEXT NOT NULL,
    `description` TEXT NOT NULL,
    `location` VARCHAR (255) NOT NULL,
    `price` INT(10) NOT NULL,
    `picture` VARCHAR (252),
    `hotel_id` int,
     KEY `FK_HotelRoom` (`hotel_id`),
    CONSTRAINT `FK_HotelRoom` FOREIGN KEY (`hotel_id`) REFERENCES `hotel`(`id`) ON DELETE SET NULL  ON UPDATE SET NULL
) DEFAULT CHARSET=utf8mb4 comment '';


DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking` (
    `id`  int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    `arrivalDate`DATE NOT NULL,
    `departureDate`DATE NOT NULL,
    `status` BOOLEAN DEFAULT 0 NOT NULL,
    `customizedPrice` INT(10) NOT NULL,
    `user_id` int,
     KEY `FK_UserBooking` (`user_id`),
    CONSTRAINT `FK_UserBooking` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE SET NULL  ON UPDATE SET NULL,
    `room_id` int,
     KEY `FK_RoomBooking` (`room_id`),
    CONSTRAINT `FK_roomBooking` FOREIGN KEY (`room_id`) REFERENCES `room`(`id`) ON DELETE SET NULL  ON UPDATE SET NULL
) DEFAULT CHARSET=utf8mb4 comment '';


DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
    `id`  int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    `paymentType` VARCHAR (255) NOT NULL,
    `description` TEXT DEFAULT NULL,
    `paymentDate`DATETIME NOT NULL,
    `status` BOOLEAN DEFAULT 0 NOT NULL,
    `total` INT (40) NULL
) DEFAULT CHARSET=utf8mb4 comment '';
