import request from "supertest";
import { connection } from "../src/repository/connection";
import { server } from "../src/server";

describe('API Hotel routes', () => {
    beforeEach(async () => {
        await connection.query('START TRANSACTION');
    })
    afterEach(async () => {
        await connection.query('ROLLBACK');
    })

    it('should return hotel list on GET', async () => {
        let response = await request(server)
            .get('/api/hotel')
            .expect(200);
        
        expect(response.body).toContainEqual({
            id: expect.any(Number),
            name: expect.any(String),
            description: expect.any(String),
            adress: expect.any(String),
            service: expect.any(String),
            type: expect.any(String),
            picture: expect.any(String),
            userId: expect.any(Number)

        });
    });

    it('should return a specific hotel on GET with id', async () => {
        let response = await request(server)
            .get('/api/hotel/1')
            .expect(200);
    
        expect(response.body).toEqual({
            id: 1,
            name: expect.any(String),
            description: expect.any(String),
            adress: expect.any(String),
            service: expect.any(String),
            type: expect.any(String),
            picture: expect.any(String),
            userId: expect.any(Number)
        });
    });

    it('should return 404 on GET with unexisting id', async () => {
        await request(server)
            .get('/api/hotel/9999')
            .expect(404);

    });


})
