import request from "supertest";
import { connection } from "../src/repository/connection";
import { server } from "../src/server";

describe('API Room routes', () => {


    beforeEach(async () => {
        await connection.query('START TRANSACTION');
    })

    afterEach(async () => {
        await connection.query('ROLLBACK');
    })



    it('should return room list on GET', async () => {
        let response = await request(server)
            .get('/api/room')
            .expect(200);
        
        expect(response.body).toContainEqual({
            id: expect.any(Number),
            name: expect.any(String),
            description: expect.any(String),
            location: expect.any(String),
            price: expect.any(Number),
            picture: expect.any(String),
            hotelId: expect.any(Number)

        });
    });

    it('should return a specific room GET with id', async () => {
        let response = await request(server)
            .get('/api/room/1')
            .expect(200);

        expect(response.body).toEqual({
            id: 1,
            name: expect.any(String),
            description: expect.any(String),
            location: expect.any(String),
            price: expect.any(Number),
            picture: expect.any(String),
            hotelId: expect.any(Number)
        });
    });

    it('should return 404 on GET with unexisting id', async () => {
        await request(server)
            .get('/api/room/9999')
            .expect(404);

    });


})
