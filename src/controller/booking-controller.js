import { Router } from "express";
import { BookingRepository } from "../repository/BookingRepository";
import { protect } from "../utils/token";
import { Booking } from "../entity/Booking";



export const bookingController = Router();

bookingController.get('/', async (req, res) => {
    try {
        let bookings;
        if (req.query.search) {
            bookings = await BookingRepository.search(req.query.search);
        } else {
            bookings = await BookingRepository.getAllBookings();
        }
        res.json(bookings)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

bookingController.get('/:id', async (req, res) => {
    try {
        res.json(await BookingRepository.findById(req.params.id))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})




bookingController.get('/user/:userId', async (req, res) => {
    try {
        res.json(await BookingRepository.findByUserId(req.params.userId))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

bookingController.get('/room/:roomId', async (req, res) => {
    try {
        res.json(await BookingRepository.findByRoomId(req.params.roomId))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})



//======================================Hotel Protected Route ================================//



bookingController.post('/',  protect(), async (req, res) => {
    try {
        const newBooking = new Booking();

        Object.assign(newBooking, req.body);


        await BookingRepository.add(newBooking, req.user);
        res.status(201).json(newBooking);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

//======================================Patch Protected Route ================================//

bookingController.patch('/:id', protect(['admin']), async (req, res) => {
    try {
        let data = await BookingRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        // if (data.userId !== req.user.id) {
        //     res.send(401).end();
        //     return
        // }
        console.log(data);
        console.log(req.body);

        let update = { ...data, ...req.body };

        await BookingRepository.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})


//====================================== Delete Protected Route ================================//

bookingController.delete('/:id', protect(['superAdmin']), async (req, res) => {
    try {
        let toDelete = await BookingRepository.findById(req.params.id);
        if (!toDelete) {
            res.status(404);
            return
        }
        if (req.user.id === toDelete.userId || req.user.role === 'superAdmin') {
            await BookingRepository.delete(toDelete.id);
            res.status(204).end();
        } else {
            res.status(401).end();
        }
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})
