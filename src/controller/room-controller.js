import { Router } from "express";
import { RoomRepository } from "../repository/RoomRepository";
import { Room } from "../entity/Room";
import { protect } from "../utils/token";
import { uploader } from "../uploader";



export const roomController = Router();

roomController.get('/', async (req, res) => {
    try {
        let rooms;
        if (req.query.search) {
            rooms = await RoomRepository.search(req.query.search);
        } else {
            rooms = await RoomRepository.getAllRooms();
        }
        res.json(rooms)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

roomController.get('/:id', async (req, res) => {
    try {
        res.json(await RoomRepository.findById(req.params.id))
    } catch (error) {
        console.log(error);
        res.status(404).end();
    }
})


roomController.get('/hotel/:hotelId', async (req, res) => {
    try {
        res.json(await RoomRepository.findByHotelId(req.params.hotelId))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

roomController.get('/search/:val', async (req, res) => {
    try {
        res.json(await RoomRepository.search(req.params.val))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

roomController.get('/room-booking/:roomId', async (req, res) => {
    try {
        res.json(await  RoomRepository.findBookingByRoomId(req.params.roomId))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})


//======================================Hotel Protected Route ================================//

roomController.post('/', uploader.single('picture'), protect(['superAdmin', 'admin']), async (req, res) => {
    try {
        const newRoom = new Room();
         // Assigning req.body to New Post
        Object.assign(newRoom, req.body);

        newRoom.picture = '/uploads/' + req.file.filename;
    


        await RoomRepository.add(newRoom, req.user);
        res.status(201).json(newRoom);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

//======================================Patch Protected Route ================================//

roomController.patch('/:id', protect(['superAdmin', 'admin']), async (req, res) => {
    try {
        let data = await RoomRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        // if (data.userId !== req.user.id) {
        //     res.send(401).end();
        //     return
        // }
        console.log(data);
        console.log(req.body);

        let update = { ...data, ...req.body };

        await RoomRepository.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})


//====================================== Delete Protected Route ================================//

roomController.delete('/:id', protect(['superAdmin']), async (req, res) => {
    try {
        let toDelete = await RoomRepository.findById(req.params.id);
        if (!toDelete) {
            res.status(404);
            return
        }
        if (req.user.id === toDelete.userId || req.user.role === 'superAdmin') {
            await RoomRepository.delete(toDelete.id);
            res.status(204).end();
        } else {
            res.status(401).end();
        }
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})
