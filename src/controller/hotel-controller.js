import { Router } from "express";
import { HotelRepository } from "../repository/HotelRepository";
import { Hotel } from "../entity/Hotel";
import { protect } from "../utils/token";
import { uploader } from "../uploader";


export const hotelController = Router();

hotelController.get('/', async (req, res) => {
    try {
        let hotels;
        if (req.query.search) {
            hotels = await HotelRepository.search(req.query.search);
        } else {
            hotels = await HotelRepository.getAllHotels();
        }
        res.json(hotels)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

hotelController.get('/:id', async (req, res) => {
    try {
        res.json(await HotelRepository.findById(req.params.id))
    } catch (error) {
        console.log(error);
        res.status(404).end();
    }
})


hotelController.get('/user/:userId', async (req, res) => {
    try {
        res.json(await HotelRepository.findByUserId(req.params.userId))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})



hotelController.get('/test/:userId', async (req, res) => {
    try {
        res.json(await HotelRepository.findRoomByUserId(req.params.userId))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})



//======================================Hotel Protected Route ================================//

hotelController.post('/', uploader.single('picture'), protect(['superAdmin', 'admin']), async (req, res) => {
    try {
        const newHotel = new Hotel();
         // Assigning req.body to New Post
        Object.assign(newHotel, req.body);

        newHotel.picture = '/uploads/' + req.file.filename;
    


        await HotelRepository.add(newHotel, req.user);
        res.status(201).json(newHotel);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

//======================================Patch Protected Route ================================//

hotelController.patch('/:id', protect(['superAdmin', 'admin']), async (req, res) => {
    try {
        let data = await HotelRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        // if (data.userId !== req.user.id) {
        //     res.send(401).end();
        //     return
        // }
        // console.log(data);
        // console.log(req.body);

        let update = { ...data, ...req.body };

        await HotelRepository.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})


//====================================== Delete Protected Route ================================//

hotelController.delete('/:id', protect(['superAdmin']), async (req, res) => {
    try {
        let toDelete = await HotelRepository.findById(req.params.id);
        if (!toDelete) {
            res.status(404);
            return
        }
        if (req.user.id === toDelete.userId || req.user.role === 'superAdmin') {
            await HotelRepository.delete(toDelete.id);
            res.status(204).end();
        } else {
            res.status(401).end();
        }
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})
