import { Router } from "express";
import { PaymentRepository } from "../repository/PaymentRepository";
import { Hotel } from "../entity/Hotel";
import { protect } from "../utils/token";
import { uploader } from "../uploader";
import { Payment } from "../entity/Payment";



export const paymentController = Router();

paymentController.get('/', async (req, res) => {
    try {
        let payments;
        if (req.query.search) {
            payments = await PaymentRepository.search(req.query.search);
        } else {
            payments = await PaymentRepository.getAllPayments();
        }
        res.json(payments)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

paymentController.get('/:id', async (req, res) => {
    try {
        res.json(await PaymentRepository.findById(req.params.id))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})



//======================================Hotel Protected Route ================================//


paymentController.post('/',  protect(), async (req, res) => {
    try {
        const newPayment = new Payment();

        Object.assign(newPayment, req.body);


        await PaymentRepository.add(newPayment, req.user);
        res.status(201).json(newPayment);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

//======================================Patch Protected Route ================================//

paymentController.patch('/:id', protect(), async (req, res) => {
    try {
        let data = await PaymentRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        // if (data.userId !== req.user.id) {
        //     res.send(401).end();
        //     return
        // }
        console.log(data);
        console.log(req.body);

        let update = { ...data, ...req.body };

        await PaymentRepository.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})


//====================================== Delete Protected Route ================================//

paymentController.delete('/:id', protect(), async (req, res) => {
    try {
        let toDelete = await PaymentRepository.findById(req.params.id);
        if (!toDelete) {
            res.status(404);
            return
        }
        if (req.user.id === toDelete.userId || req.user.role === 'admin') {
            await PaymentRepository.delete(toDelete.id);
            res.status(204).end();
        } else {
            res.status(401).end();
        }
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})
