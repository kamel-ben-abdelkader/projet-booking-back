import { Router } from "express";
import { User } from "../entity/User";
import { UserRepository } from "../repository/UserRepository";
import bcrypt from 'bcrypt';
import { generateToken, protect } from "../utils/token";
import passport from "passport";
import Joi from 'joi';

export const userController = Router();

userController.post('/', async (req, res) => {
    try {
        // Validation with Joi
        
        const schema = Joi.object({
            name: Joi.string().required(),
            firstname: Joi.string().required(),
            email: Joi.string().email().required(),
            password: Joi.string().min(4).required(),
            adress: Joi.string().required(),
            phone: Joi.string(),
            company: Joi.string(),
            role: Joi.string(),
            siret: Joi.string()
        });
        
        const { error } = schema.validate(req.body, { abortEarly: false });

        if (error) {
            res.status(400).json({ error: error.details });
            return;
        }

        const newUser = new User();
        Object.assign(newUser, req.body);

        const exists = await UserRepository.findByEmail(newUser.email);
        if (exists) {
            res.status(400).json({ error: 'Email already taken' });
            return;
        }
        //Default user role assignment for registration
        if (req.body.role ==='admin') {
            newUser.role = 'admin';
        }
        else
        newUser.role = 'user';

        // password hashing
        newUser.password = await bcrypt.hash(newUser.password, 11);

        await UserRepository.add(newUser);
        res.status(201).json(newUser);

        

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});



userController.post('/login', async (req, res) => {
    try {

        const user = await UserRepository.findByEmail(req.body.email);
        if (user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if (samePassword) {
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        id: user.id,
                        role: user.role
                    })
                });
                return;
            }
        }
        res.status(401).json({ error: 'Wrong email and/or password' });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

    // Route protection by a JWT
userController.get('/account', passport.authenticate('jwt', { session: false }), (req, res) => {

    //With req.user I access the instance of User corresponding to the token
    res.json(req.user);
});


//====================================== PATCH  Protected Route =================================//


userController.patch('/:id', protect(), async (req, res) => {
    try {
        let data = await UserRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        let update = { ...data, ...req.body };

        await UserRepository.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})
