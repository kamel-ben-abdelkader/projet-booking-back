
export class Room {
    id;
    name;
    description;
    location;
    price;
    picture;
    hotelId;

/**
 * @param {string} name
 * @param {string} description 
 * @param {string} location
 * @param {number} price
 * @param {string} picture 
 * @param {number} hotelId 
 * @param {number} id 
 */
    constructor(name, description, location, price, picture = null, hotelId, id) {
        this.name = name;
        this.description = description;
        this.location = location;
        this.price = price;
        this.picture = picture;
        this.hotelId = hotelId;
        this.id = id;
    }
}
