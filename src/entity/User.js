
export class User {
    id;
    firstname;
    name;
    company;
    email;
    password;
    adress;
    phone;
    role;
    siret;

    /**
     * 
     * @param {string} firstname
     * @param {string} name
     * @param {string} company
     * @param {string} email 
     * @param {string} password 
     * @param {string} adress
     * @param {number} phone
     * @param {string} role
     * @param {number} siret 
     * @param {number} id 
     */
    constructor(firstname, name, company, email, password, adress, phone, role='user', siret,  id = null) {
        this.firstname = firstname;
        this.name = name;
        this.company = company;
        this.email = email;
        this.password = password;
        this.adress = adress;
        this.phone = phone;
        this.role = role;
        this.siret = siret;
        this.id = id;

    }
//Objet to Json, Voluntary oversight of the password to ensure that it circulates as little as possible
    toJSON() {
        return {
            id: this.id,
            email: this.email,
            role: this.role,
            firstname: this.firstname,
            name: this.name,
            adress: this.adress,
            phone: this.phone
        }
    }
}
