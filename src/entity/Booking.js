
export class Booking {
    id;
    arrivalDate;
    departureDate;
    status;
    customizedPrice;
    userId;
    roomId;
  
/**
 * 
 * @param {Date} arrivalDate
 * @param {Date} departureDate
 * @param {Boolean} status
 * @param {number} customizedPrice
 * @param {number} userId 
 * @param {number} roomId 
 * @param {number} id 
 */
    constructor(arrivalDate, departureDate, status = false, customizedPrice, userId, roomId, id) {
        this.arrivalDate = arrivalDate;
        this.departureDate = departureDate;
        this.status = status;
        this.customizedPrice = customizedPrice;
        this.userId = userId;
        this.roomId = roomId;
        this.id = id;
    }
}
