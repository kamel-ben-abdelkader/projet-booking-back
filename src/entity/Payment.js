
export class Payment {
    id;
    paymentType;
    description;
    paymentDate;
    status;
    Total;
  
/**
 * 
 * @param {String} paymentType
 * @param {String} description 
 * @param {Date} paymentDate
 * @param {Boolean} status
 * @param {Number} total
 * @param {Number} id 
 */
    constructor(paymentType, description, paymentDate, status = false, total, id) {
        this.paymentType = paymentType;
        this.description = description;
        this.paymentDate = paymentDate;
        this.status = status;
        this.total = total;
        this.id = id;
    }
}
