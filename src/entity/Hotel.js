
export class Hotel {
    id;
    name;
    description;
    adress;
    service;
    type;
    picture;
    userId;
    // user;
/**
 * 
 * @param {string} name
 * @param {string} description 
 * @param {string} adress
 * @param {string} service
 * @param {string} type
 * @param {string} picture 
 * @param {number} userId 
 * @param {number} id 
 */
    constructor(name,description, adress, service, type , picture = null, userId, id) {
        this.name = name;
        this.description = description;
        this.adress = adress;
        this.service = service;
        this.type = type;
        this.picture = picture;
        this.userId = userId;
        this.id = id;
    }
}
