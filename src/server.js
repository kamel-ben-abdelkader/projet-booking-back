import express from 'express';
import cors from 'cors';
import { userController } from './controller/user-controller';
import { hotelController } from './controller/hotel-controller';
import { configurePassport } from './utils/token';
import passport from 'passport';
import { roomController } from './controller/room-controller';
import { bookingController } from './controller/booking-controller';




export const server = express();

server.use(cors());
configurePassport();
server.use(passport.initialize());
server.use(express.json());
server.use(express.static('public'));

server.use('/api/user', userController);
server.use('/api/hotel', hotelController);
server.use('/api/room', roomController);
server.use('/api/booking', bookingController);


