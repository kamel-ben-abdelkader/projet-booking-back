import jwt from 'jsonwebtoken';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserRepository } from '../repository/UserRepository';



export const ROLES = {
    Admin: 'admin',
    User: 'user'
}

export function generateToken(payload, expire = 60*60) {
    const token = jwt.sign(payload, process.env.JWT_SECRET,{expiresIn: expire });
    return token;
}


export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    }, async (payload, done) => {


        try {
            const user = await UserRepository.findByEmail(payload.email);

            if (user) {

                return done(null, user);
            }

            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);
        }
    }))

}

/**
 * Function to protect a route
 * @param {string[]} role an array of roles allowed to access the route (if any, then any role can access it)
 * @returns  array of middleware jwt
 */
export function protect(role = ['user', 'admin', 'superAdmin']) {

    return [
        passport.authenticate('jwt', { session: false }),
        (req, res, next) => {
            if (req.user.role === 'superAdmin' || role.includes(req.user.role)) {
                next()
            } else {
                res.status(403).json({ error: 'Access denied' });
            }
        }
    ]
}
