import multer from 'multer';
import fs from 'fs'
import path from 'path';

const storage = multer.diskStorage({
    async destination(req, file, cb) {
        
        const uploadFolder = __dirname + '/../public/uploads';
        if (!fs.existsSync(uploadFolder)) {
            fs.mkdirSync(uploadFolder, { recursive: true });
        }

        cb(null, uploadFolder);
    },
    filename(req, file, cb) {
        cb(null, file.fieldname + '-' + new Date().getTime() + path.extname(file.originalname))
    }
});

export const uploader =  multer({storage,  limits: {fileSize: 5000000}});
