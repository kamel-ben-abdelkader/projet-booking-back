import { Booking } from "../entity/Booking";
import { Hotel } from "../entity/Hotel";
import { Room } from "../entity/Room";
import { User } from "../entity/User";

import { connection } from "./connection";

export class RoomRepository {


    static async getAllRooms() {
        const [rows] = await connection.execute('SELECT * FROM room ORDER BY name DESC');
        return rows.map(item => new Room(item['name'],item['description'], item['location'], item['price'], item['picture'], item['hotel_id'], item['id']));
    };


    static async add(room) {
        const [row] = await connection.execute('INSERT INTO room (name, description, location, price, picture, hotel_id) VALUES (?,?,?,?,?,?)', [room.name, room.description, room.location, room.price, room.picture , room.hotelId]);
        console.log(room.name, room.description, room.location, room.price, room.picture , room.hotelId);
        return room.id = row.insertId;
      
        
    };

    static async delete(id) {
        await connection.execute(`DELETE FROM room WHERE id=?`, [id]);
    };

    static async update(room) {
        await connection.query('UPDATE room SET name=?, description=?, location=?, price=?,  picture=? WHERE id=?', [room.name, room.description, room.location, room.price, room.picture , room.id]);
    };


    static async findById(id) {
        let [row] = await connection.execute(`SELECT * FROM room WHERE id=?`, [id]);
        return new Room(row[0]['name'],row[0]['description'], row[0]['location'], row[0]['price'], row[0]['picture'], row[0]['hotel_id'], row[0]['id']);
    }

    static async findByHotelId(hotelId) {
        let [rows] = await connection.execute(`SELECT * FROM room WHERE hotel_id=?`, [hotelId]);
        return rows.map(item => new Room(item['name'],item['description'], item['location'], item['price'], item['picture'], item['hotel_id'], item['id']));
    }


    static async findBookingByRoomId(roomId) {
        let [rows] = await connection.execute(`SELECT room.*, booking.*, booking.id as bookingId 
        FROM room
        LEFT JOIN booking ON room.id = booking.room_id WHERE room_id=?`, [roomId]);
        const test = [];
        for (const row of rows) {
            let room = new Room ( row.name, row.description,row.location,row.price,row.picture, row.hotel_id, row.id)
            console.log(room);
            let booking = new Booking(row.arrivalDate, row.departureDate, row.status, row.customizedPrice,row.user_id,row.room_id, row.booking_id, row.id) 
           room.booking = booking
           console.log(room.booking);
            test.push(booking)
        }
        console.log(test);
        return test;
       
    }

    static async search(search) {
        const [rows] = await connection.execute(`SELECT * FROM room WHERE location LIKE ? ORDER BY name DESC`, ['%' + search + '%']);
        return rows.map(item => new Room (item['name'],item['description'], item['location'], item['price'],item['picture'], item['hotel_id'], item['id']));
    };


}

