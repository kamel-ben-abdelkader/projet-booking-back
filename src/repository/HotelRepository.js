import { Hotel } from "../entity/Hotel";
import { Room } from "../entity/Room";
import { User } from "../entity/User";
import { connection } from "./connection";

export class HotelRepository {




    static async getAllHotels() {
        const [rows] = await connection.execute('SELECT * FROM hotel ORDER BY name DESC');
        return rows.map(item => new Hotel(item['name'],item['description'], item['adress'], item['service'], item['type'], item['picture'], item['user_id'], item['id']));
    };


      //======================================== Method FindAll with join ====================================//

    // /**
    //  * Find every rows in operations table
    //  * @returns 
    //  */
    //  static async findAllPost() {


    //     const [rows] = await connection.execute({ sql: 'SELECT * FROM post LEFT JOIN user ON post.user_id = user.id', nestTables: true });
    //     console.log(rows);
    //     const posts = [];
    //     for (const row of rows) {
    //         let post = new Post(row.post.date, row.post.auteur, row.post.titre, row.post.texte, row.post.picture, row.post.userId, row.post.id);
    //         post.user = new User(row.user.nom, rows.user.prenom, rows.user.email, rows.user.password, rows.user.role, row.user.id)


    //         posts.push(post);

    //     }
    //        console.log(posts);
    //     return posts;


    // }

    static async add(hotel, user) {
        const [row] = await connection.execute('INSERT INTO hotel (name, description, adress, service, type, picture, user_id) VALUES (?,?,?,?,?,?,?)', [hotel.name, hotel.description, hotel.adress, hotel.service, hotel.type, hotel.picture, user.id]);


        return hotel.id = row.insertId;
      
        
    };

    static async delete(id) {
        await connection.execute(`DELETE FROM hotel WHERE id=?`, [id]);
    };

    static async update(hotel) {
        await connection.query('UPDATE hotel SET name=?, description=?, adress=?, service=?, type=?, picture=? WHERE id=?', [hotel.name, hotel.description, hotel.adress, hotel.service, hotel.type, hotel.picture , hotel.id]);
    };


    static async findById(id) {
        let [row] = await connection.execute(`SELECT * FROM hotel WHERE id=?`, [id]);
        return new Hotel(row[0]['name'],row[0]['description'], row[0]['adress'], row[0]['service'], row[0]['type'], row[0]['picture'], row[0]['user_id'], row[0]['id']);
    }

    static async findByUserId(userId) {
        let [rows] = await connection.execute(`SELECT * FROM hotel WHERE user_id=?`, [userId]);
        return rows.map(item => new Hotel(item['name'],item['description'], item['adress'], item['service'], item['type'], item['picture'], item['user_id'], item['id']));
    }

    static async search(search) {
        const [rows] = await connection.execute(`SELECT * FROM hotel WHERE CONCAT(name, adress) LIKE ? ORDER BY name DESC`, ['%' + search + '%']);
        return rows.map(item => new Hotel(item['name'],item['description'], item['adress'], item['service'], item['type'], item['picture'], item['user_id'], item['id']));
    };

    static async findRoomByUserId(userId) {
        let [rows] = await connection.execute(`SELECT hotel.*, room.*, room.id as roomId 
        FROM hotel
        LEFT JOIN room ON hotel.id = room.hotel_id WHERE user_id=?`, [userId]);
        const test = [];
        for (const row of rows) {
            let hotel = new Hotel(row.name, row.description,row.adress,row.service, row.type, row.picture, row.user_id, row.id)
            let room = new Room(row.name,row.description,row.location,row.price,row.picture,row.hotel_id, row.id) 
            console.log(room);
           hotel.room = room
            test.push(room)
        }
        return test;
       
    }
    


}

