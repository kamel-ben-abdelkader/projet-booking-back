import { User } from "../entity/User";
import { connection } from "./connection";


export class UserRepository {

    static async add(user) {
        const [rows] = await connection.query('INSERT INTO user (firstname, name, company, email, password, adress, phone, role, siret) VALUES (?,?,?,?,?,?,?,?,?)', [user.firstname, user.name, user.company, user.email, user.password, user.adress, user.phone, user.role, user.siret]);
        user.id = rows.insertId;
    }

    static async findByEmail(email) {
        const [rows] = await connection.query('SELECT * FROM user WHERE email=?', [email]);
        if (rows.length === 1) {
            return new User( rows[0].firstname, rows[0].name, rows[0].company, rows[0].email, rows[0].password, rows[0].adress, rows[0].phone, rows[0].role, rows[0].siret, rows[0].id);
        }
        return null;
    }

    static async delete(id) {
        await connection.execute(`DELETE FROM user WHERE id=?`, [id])
    };

    static async update(user) {
        await connection.execute('UPDATE user SET firstname=?, name=?, company=?, email=?, password=?,  adress=?, phone=?, role=?, siret=? WHERE id=?', [user.firstname, user.name, user.company, user.email, user.password, user.adress, user.phone, user.role, user.siret, user.id]);
    };


    static async findById(id) {
        let [rows] = await connection.execute(`SELECT * FROM user WHERE id=?`, [id]);
        return new User(rows[0].firstname, rows[0].name, rows[0].company, rows[0].email, rows[0].password, rows[0].adress, rows[0].phone, rows[0].role, rows[0].siret, rows[0].id);
    }

}
