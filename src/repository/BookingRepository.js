
import { Booking } from "../entity/Booking";
import { connection } from "./connection";

export class BookingRepository {




    static async getAllBookings() {
        const [rows] = await connection.execute('SELECT * FROM booking ORDER BY arrivalDate DESC');
        return rows.map(item => new Booking(item['arrivalDate'],item['departureDate'], item['status'], item['customizedPrice'], item['user_id'], item['room_id'], item['id']));
    };

    static async add(booking, user) {
        const [row] = await connection.execute('INSERT INTO booking (arrivalDate, departureDate, status, customizedPrice, user_id, room_id) VALUES (?,?,?,?,?,?)', [booking.arrivalDate, booking.departureDate,booking.status,booking.customizedPrice, user.id, booking.roomId]);


        return booking.id = row.insertId;
      
        
    };

    static async delete(id) {
        await connection.execute(`DELETE FROM booking WHERE id=?`, [id]);
    };

    static async update(booking) {
        await connection.query('UPDATE booking SET arrivalDate=?, departureDate=?, status=?, customizedPrice=? WHERE id=?', [booking.arrivalDate, booking.departureDate,booking.status,booking.customizedPrice, booking.id]);
    };


    static async findById(id) {
        let [row] = await connection.execute(`SELECT * FROM booking WHERE id=?`, [id]);
        return new Booking(row[0]['arrivalDate'],row[0]['departureDate'], row[0]['status'], row[0]['customizedPrice'],  row[0]['user_id'], row[0]['room_id'], row[0]['id']);
    }

    static async findByUserId(userId) {
        let [rows] = await connection.execute(`SELECT * FROM booking WHERE user_id=?`, [userId]);
        return rows.map(item => new Booking(item['arrivalDate'],item['departureDate'], item['status'], item['customizedPrice'], item[''], item['user_id'], item['room_id'], item['id']));
    }


    static async findByRoomId(roomId) {
        let [rows] = await connection.execute(`SELECT * FROM booking WHERE room_id=?`, [roomId]);
        return rows.map(item => new Booking(item['arrivalDate'],item['departureDate'], item['status'], item['customizedPrice'], item[''], item['user_id'], item['room_id'], item['id']));
    }


    // static async search(search) {
    //     const [rows] = await connection.execute(`SELECT * FROM hotel WHERE CONCAT(name, adress) LIKE ? ORDER BY name DESC`, ['%' + search + '%']);
    //     return rows.map(item => new Hotel(item['name'],item['description'], item['adress'], item['service'], item['type'], item['picture'], item['user_id'], item['id']));
    // };


}

