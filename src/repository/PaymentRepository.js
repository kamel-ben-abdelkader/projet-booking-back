import { Hotel } from "../entity/Hotel";
import { User } from "../entity/User";
import { connection } from "./connection";

export class PaymentRepository {




    static async getAllPayments() {
        const [rows] = await connection.execute('SELECT * FROM payment ORDER BY paymentDate DESC');
        return rows.map(item => new Hotel(item['paymentType'],item['description'], item['paymentDate'], item['status'], item['total'], item['id']));
    };


      //======================================== Method FindAll with join ====================================//

    // /**
    //  * Find every rows in operations table
    //  * @returns 
    //  */
    //  static async findAllPost() {


    //     const [rows] = await connection.execute({ sql: 'SELECT * FROM post LEFT JOIN user ON post.user_id = user.id', nestTables: true });
    //     console.log(rows);
    //     const posts = [];
    //     for (const row of rows) {
    //         let post = new Post(row.post.date, row.post.auteur, row.post.titre, row.post.texte, row.post.picture, row.post.userId, row.post.id);
    //         post.user = new User(row.user.nom, rows.user.prenom, rows.user.email, rows.user.password, rows.user.role, row.user.id)


    //         posts.push(post);

    //     }
    //        console.log(posts);
    //     return posts;


    // }

    static async add(payment) {
        const [row] = await connection.execute('INSERT INTO payment (paymentType, description, paymentDate, status, total) VALUES (?,?,?,?,?)', [payment.paymentType, payment.description, payment.paymentDate, payment.status, payment.total]);

        return payment.id = row.insertId;
      
        
    };

    static async delete(id) {
        await connection.execute(`DELETE FROM payment WHERE id=?`, [id]);
    };

    static async update(payment) {
        await connection.query('UPDATE payment SET paymentType=?, description=?, paymentDate=?, status=?, total=? WHERE id=?', [payment.paymentType, payment.description, payment.paymentDate, payment.status, payment.total , payment.id]);
    };


    static async findById(id) {
        let [row] = await connection.execute(`SELECT * FROM payment WHERE id=?`, [id]);
        return new Hotel(row[0]['paymentType'],row[0]['description'], row[0]['paymentDate'], row[0]['status'], row[0]['total'], row[0]['id']);
    }

    // static async findByUserId(userId) {
    //     let [rows] = await connection.execute(`SELECT * FROM hotel WHERE user_id=?`, [userId]);
    //     return rows.map(item => new Hotel(item['name'],item['description'], item['adress'], item['service'], item['type'], item['picture'], item['user_id'], item['id']));
    // }

    // static async search(search) {
    //     const [rows] = await connection.execute(`SELECT * FROM hotel WHERE CONCAT(name, adress) LIKE ? ORDER BY name DESC`, ['%' + search + '%']);
    //     return rows.map(item => new Hotel(item['name'],item['description'], item['adress'], item['service'], item['type'], item['picture'], item['user_id'], item['id']));
    // };


}

